import java.util.*;
import java.util.concurrent.LinkedBlockingQueue;

public class Kernel {

    private static int time = 0;

    private static int quantum = 50;
    private static int periodNeed = quantum;

    private static Kernel ourInstance = new Kernel();

    private static ArrayList<Process> jobs = new ArrayList<>();


    private static Queue<Process> readyQ = new LinkedBlockingQueue<>();

    private static Queue<Process> memWaitQ = new LinkedBlockingQueue<>();

    private static ArrayList<Process> waitingList = new ArrayList<>();

    private static boolean cpuIsBusy;
    private static Process currentProc;

    private static long qBegin = 0;


    private static Kernel getInstance() {
        return ourInstance;
    }

    private Kernel() {
    }


    public static void main(String[] args) {

        initiate();
        System.out.println("    // initiating done : " + jobs.size() + " processes");

        while (true) {

            finishIO();
//                      long term schedule
            longTermSchedule();

//                      memwait to ready
            memWaitQToReady();


            if (time - qBegin < periodNeed) {
            } else {

//                      free up (cpu) and (memory if finished or io)
                if (currentProc != null) {
                    freeUpCpu();
                    System.out.println("    # freeing cpu done on time : " + time);
                }

            }

//                      ready to cpu
            if (!readyQ.isEmpty()) {
                readyToCPU();

            }

            time++;
            /*try {
                Thread.sleep(10);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }*/


            if (jobs.size() == 0) {
                System.out.println("    +++ finished at time " + time);
                break;
            }
        }
    }


    private static void initiate() {
        Scanner s = new Scanner(System.in);
        String input = "";
        while (!input.equals("end")) {
            input = s.nextLine();
            if (input.equals("end"))
                break;
            Process tempProc = parse(input);
            for (Process p : jobs) {
                if (p.getName().equals(tempProc.getName())) {
                    System.out.println("name already used, try again");
                    continue;
                }
            }
            jobs.add(tempProc);
        }
    }


    private static void freeUpCpu() {

        currentProc.setRemainedBurstTime(currentProc.getRemainedBurstTime() - periodNeed);
        if (currentProc.goesToIO()) {
            waitingList.add(currentProc);
            System.out.println("    *process " + currentProc.getName() + " started waiting for i/0 in time " + time);
            currentProc.setIOWaitBegin(time);
            if (!memWaitQ.isEmpty()) {
                MainMemory.getMainMemory().release(currentProc);
                currentProc.setMyMemorySegment(null);
            }
        }

        if (currentProc.getRemainedBurstTime() <= 0) {
            MainMemory.getMainMemory().release(currentProc);
            System.out.println("    $$$ process " + currentProc.getName() + " done on time " + time);
            jobs.remove(currentProc);
        } else if (!currentProc.goesToIO()) {
            readyQ.add(currentProc);
        }
        if (currentProc.goesToIO()) currentProc.setGoesToIO(false);
        cpuIsBusy = false;
        currentProc = null;
    }


    private static void finishIO() {
        for (int i = 0; i < waitingList.size(); i++) {
            if (time - waitingList.get(i).getIOWaitBegin() >= waitingList.get(i).getBurstTime() / 10) {
                waitingList.get(i).setGoesToIO(false);

                if (waitingList.get(i).getMyMemorySegment() == null)
                    memWaitQ.add(waitingList.get(i));
                else readyQ.add(waitingList.get(i));

                System.out.println("    ***process " + waitingList.get(i).getName() + " finished waiting for i/0 in time " + time);
                waitingList.remove(i);
                i--;
            }
        }
    }

    private static void longTermSchedule() {
        for (Process job : jobs) {
            if (job.getArrivalTime() == time) {
                if (!memWaitQ.contains(job))
                    memWaitQ.add(job);
            }
        }
    }


    private static void memWaitQToReady() {
        while (!memWaitQ.isEmpty()) {
            Process p = memWaitQ.poll();
            if (MainMemory.getMainMemory().insertBestFit(p)) {
                readyQ.add(p);
                printProcessEnterance(p);
            } else {
                memWaitQ.add(p);
            }
        }
    }


    private static void printProcessEnterance(Process p) {
        String beginHex = Long.toHexString(p.getMyMemorySegment().getBeginByteNumber()).toUpperCase();
        String endHex = Long.toHexString((p.getMyMemorySegment().getBeginByteNumber() + p.getMyMemorySegment().getSegSize() - 1)).toUpperCase();
        System.out.println(p.getName() + " " + time + " " + beginHex + " " + endHex);
    }

    private static void readyToCPU() {
        if (!cpuIsBusy) {
            Process p = readyQ.poll();
            cpuIsBusy = true;
            currentProc = p;

            //advanced io part
            int ioTime = p.getBurstTime() / 2;

            int t = p.getRemainedBurstTime() - ioTime;
            //advanced io part


            periodNeed = Math.min(p.getRemainedBurstTime(), quantum);

            if (t <= periodNeed && t > 0) {
                p.setGoesToIO(true);
                periodNeed = t;
            }
            System.out.println("    ### cpu allocation done on time : " + time + " , given to process:" + currentProc.getName());
            qBegin = time;
        }
    }


    private static Process parse(String in) {
        int arrive, burst;
        long mem;
        String name;
        int tempIndex0 = 0;
        int tempIndex1 = in.indexOf(" ", tempIndex0);

        name = in.substring(0, tempIndex1);

        tempIndex0 = tempIndex1 + 1;
        tempIndex1 = in.indexOf(" ", tempIndex0);

        arrive = Integer.parseInt(in.substring(tempIndex0, tempIndex1));


        tempIndex0 = tempIndex1 + 1;
        tempIndex1 = in.indexOf(" ", tempIndex0);

        burst = Integer.parseInt(in.substring(tempIndex0, tempIndex1));

        tempIndex0 = tempIndex1 + 1;
        tempIndex1 = in.length();

        mem = Long.parseLong(in.substring(tempIndex0, tempIndex1));

        return new Process(name, arrive, burst, mem);
    }
}

