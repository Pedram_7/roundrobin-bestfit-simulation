public class Process {

    private String name;
    private int arrivalTime;
    private int burstTime;
    private long memoryNeed;
    private int remainedBurstTime;
    private int IOWaitBegin;
    private Segment myMemorySegment;

    private boolean goesToIO = false;

    public Process(String name, int arrivalTime, int burstTime, long memoryNeed) {
        this.name = name;
        this.arrivalTime = arrivalTime;
        this.burstTime = burstTime;
        this.memoryNeed = memoryNeed;
        this.remainedBurstTime = this.burstTime;
    }




    //getters setters

    public int getRemainedBurstTime() {
        return remainedBurstTime;
    }

    public void setRemainedBurstTime(int remainedBurstTime) {
        this.remainedBurstTime = remainedBurstTime;
    }

    public Segment getMyMemorySegment() {
        return myMemorySegment;
    }

    public void setMyMemorySegment(Segment myMemorySegment) {
        this.myMemorySegment = myMemorySegment;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getArrivalTime() {
        return arrivalTime;
    }

    public void setArrivalTime(int arrivalTime) {
        this.arrivalTime = arrivalTime;
    }

    public int getBurstTime() {
        return burstTime;
    }

    public void setBurstTime(int burstTime) {
        this.burstTime = burstTime;
    }

    public long getMemoryNeed() {
        return memoryNeed;
    }

    public void setMemoryNeed(long memoryNeed) {
        this.memoryNeed = memoryNeed;
    }

    public boolean goesToIO() {
        return goesToIO;
    }

    public void setGoesToIO(boolean goesToIO) {
        this.goesToIO = goesToIO;
    }

    public int getIOWaitBegin() {
        return IOWaitBegin;
    }

    public void setIOWaitBegin(int IOWaitBegin) {
        this.IOWaitBegin = IOWaitBegin;
    }
}
