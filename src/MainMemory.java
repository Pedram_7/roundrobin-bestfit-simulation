import java.util.ArrayList;

public class MainMemory {

    private long totalSpace;
    private long maxFreeSpace;

    private ArrayList<Segment> segments = new ArrayList<>();

    private static MainMemory mainMemory = new MainMemory();

    public static MainMemory getMainMemory() {
        return mainMemory;
    }

    public MainMemory() {
        totalSpace = 1024*8;
        totalSpace *=1024;
        totalSpace *=1024;
        maxFreeSpace = totalSpace;
        segments.add(new Segment(0,totalSpace,true));
    }



    public boolean insertBestFit(Process inputProc){
        if(maxFreeSpace<inputProc.getMemoryNeed())
            return false;
        else {
            long minPossibleSegmentSize= Long.MAX_VALUE;
            int minPossibleSegmentIndex = -1;
            for (int i=0;i<segments.size();i++) {
                if(segments.get(i).isFree() && segments.get(i).getSegSize()>inputProc.getMemoryNeed()){
                    if(segments.get(i).getSegSize()<minPossibleSegmentSize){
                        minPossibleSegmentSize = segments.get(i).getSegSize();
                        minPossibleSegmentIndex = i;
                    }
                }
            }
            if(minPossibleSegmentIndex != -1){
                Segment newFullSeg = new Segment(segments.get(minPossibleSegmentIndex).getBeginByteNumber(),inputProc.getMemoryNeed(),false);
                segments.add(minPossibleSegmentIndex,newFullSeg);
                segments.get(minPossibleSegmentIndex+1).setBeginByteNumber(newFullSeg.getBeginByteNumber()+ newFullSeg.getSegSize());
                segments.get(minPossibleSegmentIndex+1).setSegSize(segments.get(minPossibleSegmentIndex+1).getSegSize()-newFullSeg.getSegSize());
                inputProc.setMyMemorySegment(newFullSeg);
                this.calculateMaxFreeSpace();
                return true;
            }
        }
        return false;
    }

    public void release(Process inputProc){
        inputProc.getMyMemorySegment().setFree(true);
        this.attachFreeSequences();
        this.calculateMaxFreeSpace();

    }


    private void attachFreeSequences(){
        for (int i = 0; i < segments.size()-1; i++) {
            if(segments.get(i).isFree() && segments.get(i+1).isFree()){
                segments.get(i).setSegSize(segments.get(i).getSegSize()+segments.get(i+1).getSegSize());
                segments.remove(i+1);
                i--; // to stay still on this new segment
            }
        }
    }


    private void calculateMaxFreeSpace(){
        for (Segment s : segments) {
            if(s.isFree() && s.getSegSize()>maxFreeSpace) maxFreeSpace=s.getSegSize();
        }
    }

}
