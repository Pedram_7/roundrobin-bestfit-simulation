public class Segment {
    private long beginByteNumber; //byte
    private long segSize; // byte
    private boolean isFree;



    public void setBeginByteNumber(int beginByteNumber) {
        this.beginByteNumber = beginByteNumber;
    }


    public void setSegSize(int segSize) {
        this.segSize = segSize;
    }

    public boolean isFree() {
        return isFree;
    }

    public void setFree(boolean free) {
        isFree = free;
    }

    public long getBeginByteNumber() {
        return beginByteNumber;
    }

    public void setBeginByteNumber(long beginByteNumber) {
        this.beginByteNumber = beginByteNumber;
    }

    public long getSegSize() {
        return segSize;
    }

    public void setSegSize(long segSize) {
        this.segSize = segSize;
    }

    public Segment(long beginByteNumber, long segSize, boolean isFree) {
        this.beginByteNumber = beginByteNumber;
        this.segSize = segSize;
        this.isFree = isFree;
    }
}
